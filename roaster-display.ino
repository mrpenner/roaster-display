#include <SparkFunMAX31855k.h>
#include <OneWire.h>
#include <DallasTemperature.h>
#include <SPI.h>
#include <TFT.h>

const uint8_t logswitch = 5;
const uint8_t ledpin = 6;

const uint8_t lcd_cs = 3;
const uint8_t dc = A0;
const uint8_t rst = A1;
TFT screen=TFT(lcd_cs, dc, rst);

const uint8_t CHIP_SELECT_PIN = 7;
SparkFunMAX31855k probe(CHIP_SELECT_PIN);

OneWire oneWire(4);
DallasTemperature sensors(&oneWire);
DeviceAddress Thermometer2 = { 0x3B, 0x86, 0xB2, 0x3D, 0x07, 0x98, 0x4C, 0xC9 };

const uint8_t xMax = screen.width() - 1;
const uint8_t yMax = screen.height() - 1;
const uint8_t graphHeight = screen.height() - 31;
const uint32_t graphMaxTime = 20 * 60000; // how many seconds on the whole x axis

const float alphaAT = 0.5;
const float alphaBT = 0.5;
const float alphaET = 0.4;
const float alphaRoR = 0.1;

float alpha_NaN;
uint32_t lastTime;
uint32_t startTime;
uint16_t green = 0x07e0;
uint16_t BT_color = green;
uint16_t ET_color = green;
float AT;
float BT;
float ET;
float ET_adj = 8;
float lastBT;
float RoR;

void setup() {
    pinMode(logswitch, INPUT);
    pinMode(ledpin, OUTPUT);
    
    Serial.begin(115200);

    screen.begin();
    screen.background(0, 0, 0);
    screen.fill(0, 0, 0);
    screen.setRotation(1);
    screen.setTextColor(green, 0);
    screen.setTextSize(1);
    screen.println(F("Initializing..."));

    do {
        BT = probe.readTempC();
    } while (isnan(BT));
    lastBT = BT;
    
    sensors.begin();
    sensors.setResolution(Thermometer2, 9);
    sensors.requestTemperatures();
    ET = sensors.getTempC(Thermometer2);
    if (isnan(ET)) {
        ET = 0;
    }
}

void loop() {
    uint32_t totalTime = millis();
    
    if (digitalRead(logswitch)) { 
        analogWrite(ledpin, 15);
    } 
    else {
        // clear graph and reset timer
        screen.rect(0, graphHeight, screen.width(), screen.height()-graphHeight);
        analogWrite(ledpin, 0);
        startTime = totalTime;
    }
    uint32_t elapsedTime = totalTime - startTime;
    
    float rawTemp = probe.readTempC();
    if (isnan(rawTemp)) {
        BT_color = shift(BT_color, 0);
        alpha_NaN += 0.05; // weight the latest reading heavier the more times received NaN
        if (0.9 < alpha_NaN) alpha_NaN = 0.9;
    } else {
        BT_color = shift(BT_color, 1);
        if (alpha_NaN) {  // if we have got NaN recently, weight this reading heavier
            BT = alpha_NaN * rawTemp + (1 - alpha_NaN) * BT;
            alpha_NaN = 0;
        } else {
            BT = alphaBT * rawTemp + (1 - alphaBT) * BT;
        }
        uint32_t d_time = totalTime - lastTime;
        if (500 < d_time) {
            float rawRoR = (BT - lastBT) * 60000 / d_time;
            lastTime = totalTime;
            RoR = alphaRoR * rawRoR + (1 - alphaRoR) * RoR;
            lastBT = BT;
        }
    }
    
    sensors.requestTemperatures();
    rawTemp = sensors.getTempC(Thermometer2);
    if (isnan(rawTemp)) {
        ET_color = shift(ET_color, 0);
    } else {
        ET_color = shift(ET_color, 1);
        rawTemp += ET_adj;
        ET = alphaET * rawTemp + (1 - alphaET) * ET;
    }
    
    rawTemp = probe.readCJT();
    if (!isnan(rawTemp)){
        AT = alphaAT * rawTemp + (1 - alphaAT) * AT;
    }
    if (Serial.available() > 0 && Serial.readStringUntil('\n') == "READ") {
        Serial.print(AT, 2);
        Serial.print(',');
        Serial.print(ET, 2);
        Serial.print(',');
        Serial.print(BT, 2);
        Serial.print(',');
    }
        
    uint8_t mins = (elapsedTime / 60000);
    uint8_t secs = (elapsedTime % 60000) / 1000;
    
    screen.setRotation(1);
    screen.setCursor(0, 0);
    screen.setTextSize(3);
    
    screen.setTextColor(BT_color, 0);
    screen.print(BT, 1);
    screen.println(' ');
    
    screen.setTextColor(ET_color, 0);
    screen.print(ET, 1);
    screen.println(' ');
    
    screen.setTextColor(green, 0);
    screen.print(mins);
    screen.print(':');
    if (secs < 10) screen.print('0');
    screen.print(secs);
    screen.println(' ');
    screen.print(RoR, 2);
    screen.print(' ');

    uint32_t graphTime = constrain(elapsedTime, 0, graphMaxTime);
    screen.drawPixel(map(graphTime, 0, graphMaxTime, 0, xMax),
                    map(constrain(BT, 25, 250), 25, 250, yMax, graphHeight), 0x07ff);

    screen.drawPixel(map(graphTime, 0, graphMaxTime, 0, xMax),
                    map(constrain(RoR, 0, 30), 0, 30, yMax, graphHeight), 0xffe0);
}

uint16_t shift(uint16_t color, bool dir) {
    // make redder or greener while keeping max brightness
    
    uint16_t red = color >> 11;
    uint16_t green = color >> 5;
    green &= 63;
    
    if (dir) {
        if ((green + 2) < 63) {
            green += 2;
        } else {
            green = 63;
            if (red > 1) {
                red -= 1;
            } else {
                red = 0;
            }
        }
    } else {
        if ((red + 1) < 31) {
            red += 1;
        } else {
            red = 31;
            if (green > 2) {
                green -= 2;
            } else {
                green = 0;
            }
        }
    } 
    
    red = red << 11;
    green = green << 5;
    
    color = red | green;
    
    return color;
}
