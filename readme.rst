Display for a coffee roaster
--------------------------------
An Arduino based display for displaying and graphing temperature and time, used for roasting coffee.

I don't plan to add the schematic because it's not really the way I would design it now. My ideas for it have changed some, and the electronics and code have evolved (or devolved?) over time and gotten a little messy. I would like to replace it, so don't expect many changes here. However, some of the code may be mildly interesting, so here it is.

There are two thermocouples, one for bean temperature, one for environment temperature. The text color is originally green, and the code checks for bad values from the thermocouples and shifts the text color redder as long as the reading is bad. Exponential smoothing is applied to the good readings. The rate of rise (first derivative) of the bean temperature per minute is calculated and also exponentially smoothed. The graph of the bean temperature and rate of rise is drawn by drawing a dot each loop at the time and temperature coordinates. The graph and timer can be cleared by turning a switch off, and started by turning the switch back on. It can also connect to Artisan_ running on a computer as if it was a TC4_ shield. Only the command to read temperatures is implemented, though.

.. _Artisan: https://artisan-scope.org/

.. _TC4: https://github.com/greencardigan/TC4-shield 
